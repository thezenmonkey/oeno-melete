<?php
	
class Address extends DataExtension {
	
	private static $db = array(
		"Address" => "Varchar(255)",
		"Address2" => "Varchar(255)",
		"City" => "Varchar(255)",
		"StateOrProvince" => "Varchar(255)",
		"Country" => "Varchar(255)",
		"PostalCode" => "Varchar(255)",
		"Phone" => "Varchar(255)",
		"Fax" => "Varchar(255)",
		"MainEmail" => "Varchar(255)"
	);
	
	private static $has_one = array(
		
	);
	
	private static $has_many = array(
		
	);
	
	public function updateCMSFields(FieldList $fields) {
		
/*
		if($this->owner->ClassName == "Contact") {
			
		}
*/
	
	//$fields->addFieldToTab("Root.Main", TextField::create('Address'));
		
	}
}