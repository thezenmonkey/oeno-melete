<?php
	
class GalleryCMSImage extends DataExtension {
	
	private static $db = array(
		
	);
	
	private static $has_one = array(
		
	);
	
	private static $has_many = array(
		
	);
	
	public function updateCMSFields(FieldList $fields) {
		
	}
	
	public function getRatio() {
		return $this->owner->getWidth() / $this->owner->getHeight();
	}
	
	public function SafeSetWidth($width) {
		return ($this->owner->getWidth() > $width) ? $this->owner->SetWidth($width) : $this->owner;
	}
	
	public function SafeSetHeight($height) {
		return ($this->owner->getHeight() > $height) ? $this->owner->SetHeight($height) : $this->owner;
	}
	
	public function SafeSetRatioSize($width, $height) {
		return ($this->owner->getHeight() > $height || $this->owner->getWidth() > $width) ? $this->owner->SetRatioSize($width, $height) : $this->owner;
	}
	
}