<?php
if (!class_exists("CustomerId")) {
/**
 * CustomerId
 */
class CustomerId {
	/**
	 * @access public
	 * @var integer
	 */
	public $CustId;
	/**
	 * @access public
	 * @var string
	 */
	public $Error;
}}
if (!class_exists("OrderId")) {
/**
 * OrderId
 */
class OrderId {
	/**
	 * @access public
	 * @var integer
	 */
	public $OrdId;
	/**
	 * @access public
	 * @var string
	 */
	public $Error;
}}
if (!class_exists("OrderItemId")) {
/**
 * OrderItemId
 */
class OrderItemId {
	/**
	 * @access public
	 * @var integer
	 */
	public $ItemId;
	/**
	 * @access public
	 * @var string
	 */
	public $Error;
}}
if (!class_exists("EventRecord")) {
/**
 * EventRecord
 */
class EventRecord {
	/**
	 * @access public
	 * @var integer
	 */
	public $EventId;
	/**
	 * @access public
	 * @var string
	 */
	public $EventTitle;
	/**
	 * @access public
	 * @var string
	 */
	public $StartDate;
	/**
	 * @access public
	 * @var string
	 */
	public $EndDate;
	/**
	 * @access public
	 * @var string
	 */
	public $Location;
	/**
	 * @access public
	 * @var string
	 */
	public $Featured;
	/**
	 * @access public
	 * @var string
	 */
	public $Description;
	/**
	 * @access public
	 * @var string
	 */
	public $DateAdded;
	/**
	 * @access public
	 * @var string
	 */
	public $DateUpdated;
	/**
	 * @access public
	 * @var string
	 */
	public $Error;
}}
if (!class_exists("EventRecordArray")) {
/**
 * EventRecordArray
 */
class EventRecordArray {
}}
if (!class_exists("ArtistRecord")) {
/**
 * ArtistRecord
 */
class ArtistRecord {
	/**
	 * @access public
	 * @var string
	 */
	public $LastName;
	/**
	 * @access public
	 * @var string
	 */
	public $FirstName;
	/**
	 * @access public
	 * @var integer
	 */
	public $ArtistId;
	/**
	 * @access public
	 * @var string
	 */
	public $Description;
	/**
	 * @access public
	 * @var string
	 */
	public $DateAdded;
	/**
	 * @access public
	 * @var string
	 */
	public $DateUpdated;
	/**
	 * @access public
	 * @var integer
	 */
	public $FirstTitleId;
	/**
	 * @access public
	 * @var string
	 */
	public $Born;
	/**
	 * @access public
	 * @var string
	 */
	public $Died;
	/**
	 * @access public
	 * @var integer
	 */
	public $HasBio;
	/**
	 * @access public
	 * @var integer
	 */
	public $HasImage;
	/**
	 * @access public
	 * @var string
	 */
	public $Error;
}}
if (!class_exists("ArtistRecordArray")) {
/**
 * ArtistRecordArray
 */
class ArtistRecordArray {
}}
if (!class_exists("CategoryMediumRecord")) {
/**
 * CategoryMediumRecord
 */
class CategoryMediumRecord {
	/**
	 * @access public
	 * @var string
	 */
	public $Categories;
	/**
	 * @access public
	 * @var string
	 */
	public $Mediums;
	/**
	 * @access public
	 * @var string
	 */
	public $Error;
}}
if (!class_exists("CategoryMediumRecordArray")) {
/**
 * CategoryMediumRecordArray
 */
class CategoryMediumRecordArray {
}}
if (!class_exists("CompanyRecord")) {
/**
 * CompanyRecord
 */
class CompanyRecord {
	/**
	 * @access public
	 * @var string
	 */
	public $Name;
	/**
	 * @access public
	 * @var string
	 */
	public $Address1;
	/**
	 * @access public
	 * @var string
	 */
	public $Address2;
	/**
	 * @access public
	 * @var string
	 */
	public $City;
	/**
	 * @access public
	 * @var string
	 */
	public $State;
	/**
	 * @access public
	 * @var string
	 */
	public $Zip;
	/**
	 * @access public
	 * @var string
	 */
	public $Description;
	/**
	 * @access public
	 * @var string
	 */
	public $Email;
	/**
	 * @access public
	 * @var string
	 */
	public $Hours;
	/**
	 * @access public
	 * @var string
	 */
	public $Location;
	/**
	 * @access public
	 * @var string
	 */
	public $Phone;
	/**
	 * @access public
	 * @var string
	 */
	public $Fax;
	/**
	 * @access public
	 * @var string
	 */
	public $TollFree;
	/**
	 * @access public
	 * @var string
	 */
	public $Upcoming;
	/**
	 * @access public
	 * @var string
	 */
	public $Error;
}}
if (!class_exists("CompanyRecordArray")) {
/**
 * CompanyRecordArray
 */
class CompanyRecordArray {
}}
if (!class_exists("TitleRecord")) {
/**
 * TitleRecord
 */
class TitleRecord {
	/**
	 * @access public
	 * @var integer
	 */
	public $TitleId;
	/**
	 * @access public
	 * @var string
	 */
	public $Title;
	/**
	 * @access public
	 * @var integer
	 */
	public $ArtistId;
	/**
	 * @access public
	 * @var string
	 */
	public $ScanCode;
	/**
	 * @access public
	 * @var double
	 */
	public $ArtCode;
	/**
	 * @access public
	 * @var string
	 */
	public $Price;
	/**
	 * @access public
	 * @var string
	 */
	public $Circa;
	/**
	 * @access public
	 * @var integer
	 */
	public $Images;
	/**
	 * @access public
	 * @var double
	 */
	public $Width;
	/**
	 * @access public
	 * @var double
	 */
	public $Height;
	/**
	 * @access public
	 * @var double
	 */
	public $Depth;
	/**
	 * @access public
	 * @var double
	 */
	public $Weight;
	/**
	 * @access public
	 * @var string
	 */
	public $DimUnits;
	/**
	 * @access public
	 * @var string
	 */
	public $DimType;
	/**
	 * @access public
	 * @var string
	 */
	public $Category;
	/**
	 * @access public
	 * @var string
	 */
	public $Medium;
	/**
	 * @access public
	 * @var string
	 */
	public $Subject;
	/**
	 * @access public
	 * @var string
	 */
	public $Description;
	/**
	 * @access public
	 * @var string
	 */
	public $EditionType;
	/**
	 * @access public
	 * @var integer
	 */
	public $EditionSize;
	/**
	 * @access public
	 * @var integer
	 */
	public $Quantity;
	/**
	 * @access public
	 * @var string
	 */
	public $TitleText1;
	/**
	 * @access public
	 * @var string
	 */
	public $TitleText2;
	/**
	 * @access public
	 * @var string
	 */
	public $TitleText3;
	/**
	 * @access public
	 * @var string
	 */
	public $Custom1;
	/**
	 * @access public
	 * @var string
	 */
	public $Custom2;
	/**
	 * @access public
	 * @var string
	 */
	public $Custom3;
	/**
	 * @access public
	 * @var string
	 */
	public $DateAdded;
	/**
	 * @access public
	 * @var string
	 */
	public $DateUpdated;
	/**
	 * @access public
	 * @var integer
	 */
	public $FramePrice;
	/**
	 * @access public
	 * @var integer
	 */
	public $FrameHeight;
	/**
	 * @access public
	 * @var integer
	 */
	public $FrameWidth;
	/**
	 * @access public
	 * @var string
	 */
	public $Error;
}}
if (!class_exists("TitleRecordArray")) {
/**
 * TitleRecordArray
 */
class TitleRecordArray {
}}
if (!class_exists("CustomRecord")) {
/**
 * CustomerId
 */
class CustomRecord {
	/**
	 * @access public
	 * @var string
	 */
	public $Custom;
	/**
	 * @access public
	 * @var string
	 */
	public $Error;
}}
if (!class_exists("CustomRecordArray")) {
/**
 * CompanyRecordArray
 */
class CustomRecordArray {
}}
if (!class_exists("MasterpieceService")) {
/**
 * MasterpieceService
 * @author WSDLInterpreter
 */
class MasterpieceService extends SoapClient {
	/**
	 * Default class map for wsdl=>php
	 * @access private
	 * @var array
	 */
	private static $classmap = array(
		"CustomerId" => "CustomerId",
		"OrderId" => "OrderId",
		"OrderItemId" => "OrderItemId",
		"EventRecord" => "EventRecord",
		"EventRecordArray" => "EventRecordArray",
		"ArtistRecord" => "ArtistRecord",
		"ArtistRecordArray" => "ArtistRecordArray",
		"CategoryMediumRecord" => "CategoryMediumRecord",
		"CategoryMediumRecordArray" => "CategoryMediumRecordArray",
		"CompanyRecord" => "CompanyRecord",
		"CompanyRecordArray" => "CompanyRecordArray",
		"TitleRecord" => "TitleRecord",
		"TitleRecordArray" => "TitleRecordArray",
		"CustomRecord" => "CustomRecord",
		"CustomRecordArray" => "CustomRecordArray"
	);
	/**
	 * Constructor using wsdl location and options array
	 * @param string $wsdl WSDL location for this service
	 * @param array $options Options for the SoapClient
	 */
	public function __construct($wsdl="masterpiece.wsdl", $options=array()) {
		
		// 5/30/2011 - commented this out cuz couldn't run soap funcs on web2 otherwise, seemed fine for other servers and with local test (very odd - TPR)
		
//		foreach(self::$classmap as $wsdlClassName => $phpClassName) {
//		    if(!isset($options['classmap'][$wsdlClassName])) {
//		        $options['classmap'][$wsdlClassName] = $phpClassName;
//		    }
//		]

		parent::__construct($wsdl, $options);
	}
	/**
	 * Checks if an argument list matches against a valid argument type list
	 * @param array $arguments The argument list to check
	 * @param array $validParameters A list of valid argument types
	 * @return boolean true if arguments match against validParameters
	 * @throws Exception invalid function signature message
	 */
	public function _checkArguments($arguments, $validParameters) {
		$variables = "";
		foreach ($arguments as $arg) {
		    $type = gettype($arg);
		    if ($type == "object") {
		        $type = get_class($arg);
		    }
		    $variables .= "(".$type.")";
		}
		if (!in_array($variables, $validParameters)) {
		    throw new Exception("Invalid parameter types: ".str_replace(")(", ", ", $variables));
		}
		return true;
	}
	/**
	 * Service Call: PutCustomerRecord
	 * Parameter options:
	 * (string) CompanyId, (string) FirstName, (string) LastName, (string) Email, (string) Address1, (string) Address2, (string) City, (string) State, (string) Zip, (string) Country, (string) Notes, (string) Phone, (string) Fax, (string) CompanyName
	 * @param mixed,... See function description for parameter options
	 * @return CustomerId
	 * @throws Exception invalid function signature message
	 */
	public function PutCustomerRecord($mixed = null) {
		$validParameters = array(
			"(string)(string)(string)(string)(string)(string)(string)(string)(string)(string)(string)(string)(string)(string)",
		);
		$args = func_get_args();
		$this->_checkArguments($args, $validParameters);
		return $this->__soapCall("PutCustomerRecord", $args);
	}
	/**
	 * Service Call: PutOrderRecord
	 * Parameter options:
	 * (string) CompanyId, (integer) CustId, (string) FirstName, (string) LastName, (string) Address1, (string) Address2, (string) City, (string) State, (string) Zip, (string) Country, (string) Phone, (string) Fax, (string) Notes, (double) OrderAmount, (double) Shipping, (double) Handling, (double) Insurance, (double) SalesTax
	 * @param mixed,... See function description for parameter options
	 * @return OrderId
	 * @throws Exception invalid function signature message
	 */
	public function PutOrderRecord($mixed = null) {
		$validParameters = array(
			"(string)(integer)(string)(string)(string)(string)(string)(string)(string)(string)(string)(string)(string)(double)(double)(double)(double)(double)",
		);
		$args = func_get_args();
		$this->_checkArguments($args, $validParameters);
		return $this->__soapCall("PutOrderRecord", $args);
	}
	/**
	 * Service Call: PutOrderItemRecord
	 * Parameter options:
	 * (string) CompanyId, (integer) OrdId, (integer) TitleId, (integer) Quantity, (double) Price, (string) Description
	 * @param mixed,... See function description for parameter options
	 * @return OrderItemId
	 * @throws Exception invalid function signature message
	 */
	public function PutOrderItemRecord($mixed = null) {
		$validParameters = array(
			"(string)(integer)(integer)(integer)(double)(string)",
		);
		$args = func_get_args();
		$this->_checkArguments($args, $validParameters);
		return $this->__soapCall("PutOrderItemRecord", $args);
	}
	/**
	 * Service Call: GetEventRecords
	 * Parameter options:
	 * (string) CompanyId, (string) Filter
	 * @param mixed,... See function description for parameter options
	 * @return EventRecord[]
	 * @throws Exception invalid function signature message
	 */
	public function GetEventRecords($mixed = null) {
		$validParameters = array(
			"(string)(string)",
		);
		$args = func_get_args();
		$this->_checkArguments($args, $validParameters);
		
		$records = $this->__soapCall("GetEventRecords", $args);
	
		foreach ($records as &$record)
			foreach ($record as &$r)
				$r = urldecode($r);
		
		return $records;
		
//		return $this->__soapCall("GetEventRecords", $args);
	}
	/**
	 * Service Call: GetArtistRecords
	 * Parameter options:
	 * (string) CompanyId, (string) Filter
	 * @param mixed,... See function description for parameter options
	 * @return ArtistRecord[]
	 * @throws Exception invalid function signature message
	 */
	public function GetArtistRecords($mixed = null) {
		$validParameters = array(
			"(string)(string)",
		);
		$args = func_get_args();
		$this->_checkArguments($args, $validParameters);
		$records = $this->__soapCall("GetArtistRecords", $args);
		
		foreach ($records as &$record)
			foreach ($record as &$r)
				$r = urldecode($r);
		
		return $records;
//return $this->__soapCall("GetArtistRecords", $args);
	}
	/**
	 * Service Call: GetCategoryMediumRecords
	 * Parameter options:
	 * (string) CompanyId, (string) Filter
	 * @param mixed,... See function description for parameter options
	 * @return CategoryMediumRecord[]
	 * @throws Exception invalid function signature message
	 */
	public function GetCategoryMediumRecords($mixed = null) {
		$validParameters = array(
			"(string)(string)",
		);
		$args = func_get_args();
		$this->_checkArguments($args, $validParameters);
		$records = $this->__soapCall("GetCategoryMediumRecords", $args);
		
		foreach ($records as &$record)
			foreach ($record as &$r)
				$r = urldecode($r);
		
		return $records;
//return $this->__soapCall("GetCategoryMediumRecords", $args);
	}
	/**
	 * Service Call: GetCompanyRecords
	 * Parameter options:
	 * (string) CompanyId, (string) Filter
	 * @param mixed,... See function description for parameter options
	 * @return CompanyRecord[]
	 * @throws Exception invalid function signature message
	 */
	public function GetCompanyRecords($mixed = null) {
		$validParameters = array(
			"(string)(string)",
		);
		$args = func_get_args();
		$this->_checkArguments($args, $validParameters);
		
		$records = $this->__soapCall("GetCompanyRecords", $args);
		
		foreach ($records as &$record)
			foreach ($record as &$r)
				$r = urldecode($r);
		
		return $records;
		
//		return $this->__soapCall("GetCompanyRecords", $args);
	}
	/**
	 * Service Call: GetTitleRecords
	 * Parameter options:
	 * (string) CompanyId, (string) Filter
	 * @param mixed,... See function description for parameter options
	 * @return TitleRecord[]
	 * @throws Exception invalid function signature message
	 */
	public function GetTitleRecords($mixed = null) {
		$validParameters = array(
			"(string)(string)",
		);
		$args = func_get_args();
		$this->_checkArguments($args, $validParameters);
		
		$records = $this->__soapCall("GetTitleRecords", $args);
		
//		echo "<p>";
//		var_dump($records);
//		echo "</p>";
		
		foreach ($records as &$record)
			foreach ($record as &$r)
				$r = urldecode($r);
		
		return $records;
//		return $this->__soapCall("GetTitleRecords", $args);
	}
	/**
	 * Service Call: GetCustomRecords
	 * Parameter options:
	 * (string) CompanyId, (string) Filter
	 * @param mixed,... See function description for parameter options
	 * @return TitleRecord[]
	 * @throws Exception invalid function signature message
	 */
	public function GetCustomRecords($mixed = null) {
		$validParameters = array(
			"(string)(string)",
		);
		$args = func_get_args();
		$this->_checkArguments($args, $validParameters);
		
		$records = $this->__soapCall("GetCustomRecords", $args);
		
		return $records;
	}
}}
?>