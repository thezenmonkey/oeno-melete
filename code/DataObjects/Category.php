<?php


class Category extends DataObject {
	
	private static $db = array (
		"Title" => "Varchar(255)",
		"Description" => "Text"
	);
	
	private static $has_one = array (
		
	);
	
	private static $has_many = array (
		
	);
	
}

class ArtworkCategory extends Category {
	
	private static $db = array (
		
	);
	
	private static $has_one = array (
		
	);
	
	private static $has_many = array (
		"Artworks" => "Artwork"	
	);
	
}