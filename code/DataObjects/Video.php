<?php

class Video extends DataObject {
	
	private static $db = array (
		"Title" => "Varchar",
		"Content" => "HTMLText",
		"URL" => "Varchar(255)"
	);
	
	private static $has_one = array (
		"Upload" => "File",
		"Artist" => "Artist",
		"Exhibition" => "Exhibit"
	);
	
	private static $has_many = array (
		
	);
	
	public function EmbedVideo() {
		
		if($this->URL) {
			$embedObject = Oembed::get_oembed_from_url(str_replace("https", "http", $this->URL));
			// $embedObject = Oembed::get_oembed_from_url($this->owner->VideoURL."?vq=hd720"); HD NOT FOR VIMEO
			//$embedObject = Oembed::handle_shortcode(null, $this->owner->VideoURL, 'default');
			if($embedObject) {
				return $embedObject->forTemplate();
			} else {
				return "Error Loading Video";
			}
			
		} else {
			return false;
		}
		
	}
}