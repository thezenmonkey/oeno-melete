<?php

class ArtworkEdition extends DataObject {
	
	private static $db = array (
		"EditionNumber" => "Int"
	);
	
	private static $has_one = array (
		"Artwork" => "Artwork"
	);
	
	private static $has_many = array (
		
	);
	
}