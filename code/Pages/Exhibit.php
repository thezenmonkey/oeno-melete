<?php

class Exhibit extends Page {
	
	/**
	* Static vars
	* ----------------------------------*/
	
	// private static $show_in_sitetree = false;
	

	/**
	* Object vars
	* ----------------------------------*/
	
	
	
	/**
	* Static methods
	* ----------------------------------*/
	
/*
	public function canView($member = null) {
        return Permission::check('CMS_ACCESS_ExhibitAdmin', 'any', $member);
    }
*/

    public function canEdit($member = null) {
        return Permission::check('CMS_ACCESS_ExhibitAdmin', 'any', $member);
    }

    public function canDelete($member = null) {
        return Permission::check('CMS_ACCESS_ExhibitAdmin', 'any', $member);
    }

    public function canCreate($member = null) {
        return Permission::check('CMS_ACCESS_ExhibitAdmin', 'any', $member);
    }
	
	
	/**
	* Data model
	* ----------------------------------*/

	private static $db = array (
		"StartDate" => "SS_Datetime",
		"EndDate" => "SS_Datetime",
		"Featured" => "Boolean",
		"OnGoing" => "Boolean"
	);
	

	private static $has_one = array (
		"Location" => "Location",
		"FeaturedArtwork" => "Artwork"
	);
	
	private static $has_many = array (
		
	);
	
	private static $many_many = array(
		"Artworks" => "Artwork"
	);

    private static $many_many_extraFields = array(
        "Artworks" => array(
            "EventOrder" => "Int"
        )
    );
	
	private static $belongs_many_many = array(
		"Videos" => "Video"
	);
	
	private static $better_buttons_actions = array (
        'CheckArt',
    );
    
    private static $summary_fields = array (
	   "CoverImage" => "Cover",
	   "Title" => "Exhibit Name",
	   "StartDate.Full" => "Start"
    );
	
	/**
	* Common methods
	* ----------------------------------*/
	
	public function getCMSFields() {
        $config = $this->config();

        $fields = parent::getCMSFIelds();
		
		$fields->removeFieldsFromTab("Root", array(
			"Gallery"
		));
		
		$fields->insertAfter(DateField::create("StartDate"), "Title");
		$fields->insertAfter(DateField::create("EndDate"), "StartDate");
		$fields->insertAfter(CheckboxField::create("OnGoing"), "EndDate");
		
		if($this->Artworks()->count()) {
			$fields->insertAfter(
                DropdownField::create("FeaturedArtworkID", "Featured Work", $this->Artworks()->map("ID", "Title")),
                "MenuTitle"
            );
		}
		
		$locations = Location::get()->filter(array("Public" => 1));
		if($locations->count()) {
			$fields->insertAfter(DropdownField("LocationID", "Location", $locations->map("ID", "Title")));
		}

		$artworkFieldConfig = GridFieldConfig_RelationEditor::create();

        if($config->artwork_manual_sort == true && $this->Artworks()->count()) {
            $artworkFieldConfig->addComponent(new GridFieldOrderableRows('EventOrder'));
            $artworkFieldConfig->getComponentByType('GridFieldPaginator')->setItemsPerPage(50);
        }

		$artworkField = GridField::create("Artworks", "Pieces in Show", $this->Artworks())
            ->setConfig($artworkFieldConfig);

		$fields->addFieldsToTab("Root.Artwork", $artworkField);
		
		$fields->addFieldsToTab(
            "Root.Installation",
            GalleryUploadField::create("Images",'Images',$this->OrderedImages())
                ->setFolderName('assets/Exhibitions/'.$this->URLSegment)
        );
		
		return $fields;
	}
	
	public function getBetterButtonsUtils() {
        $fields = parent::getBetterButtonsUtils();
        
        
            $fields->push(
            	BetterButtonCustomAction::create('CheckArt', 'Check Art')
            		->setRedirectType(BetterButtonCustomAction::REFRESH)
					->setSuccessMessage('Synced Please Re-Publish')
            
            );
        
        return $fields;
    }
    
    public function CheckArt() {
	   
	   if($this->MPFeatured) {
		   $artworks = Artwork::get()->filter(array("MPTitleText3:PartialMatch" => $this->MPFeatured));
		   
		   if($artworks->count()) {
				//ADD ARTWORK ONLY IF ITS NOT ALREADY THERE
				foreach ($artworks as $art) {
					if(!$this->Artworks()->find('ID', $art->ID)) {
						$this->Artworks()->add($art);
						echo  $art->Title." Added<br>";
					}
					
				}
			}
	   }
	   
	   
	   
	   return true;
    }
	
	/**
	* Accessor methods
	* ----------------------------------*/
	
	
	/**
	* Controller actions	
	* ----------------------------------*/
	
	
	
	/**
	* Template accessors
	* ----------------------------------*/
	
	public function getFirstImage() {
		
		if($this->FeaturedArtworkID != 0) {
			return $this->FeaturedArtwork()->OrderedImages()->First();
		}
		
		if($this->Artworks()->count()) {
			$image;
			foreach($this->Artworks()->sort('MPDateAdded', 'DESC') as $artwork) {
				if($artwork->OrderedImages()->First()) {
					$image = $artwork->OrderedImages()->First();
					break;
				} else {
					continue;
				}
			}
			
			return $image ? $image : false;
			
			
		} else {
			return false;
		}
	}
	
	public function getSortedArtwork() {
        if($this->config()->artwork_manual_sort == true) {
            return $this->Artworks()->sort(array('Quantity' => "DESC",'EventOrder' => 'ASC','MPDateAdded' => 'DESC'));
        } else {
            return $this->Artworks()->sort(array('Quantity' => "DESC",'MPDateAdded' => 'DESC'));
        }

	}
	
	public function getCoverImage() {
		$image = $this->getFirstImage();
		
		return $image ? $image->setWidth(150) : false;
	}
	
	
	/**
	* Object methods
	* ----------------------------------*/

	

	
}


class Exhibit_Controller extends Page_Controller {
	
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
		
	}
	
}