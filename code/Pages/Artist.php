<?php
	
class Artist extends Page {
	
	/**
	 * Static vars
	 * ----------------------------------*/
	
	

	/**
	 * Object vars
	 * ----------------------------------*/
	
	
	
	/**
	 * Static methods
	 * ----------------------------------*/


    public function canEdit($member = null) {
        return Permission::check('CMS_ACCESS_ArtistAdmin', 'any', $member);
    }

    public function canDelete($member = null) {
        return Permission::check('CMS_ACCESS_ArtistAdmin', 'any', $member);
    }

    public function canCreate($member = null) {
        return Permission::check('CMS_ACCESS_ArtistAdmin', 'any', $member);
    }
	
	
	/**
	 * Data model
	 * ----------------------------------*/

	private static $db = array (
		"MiddleName" => "Varchar",
		"BirthDate" => "SS_DateTime",
		"DeathDate" => "SS_DateTime",
		"ShortBio" => "HTMLText",
		"IsRepresented" => "Boolean"
	);

	private static $has_one = array (
		"Contact" => "Contact",
		"Picture" => "Image",
		"BlogTag" => "BlogTag"
	);
	
	private static $has_many = array (
		"Artworks" => "Artwork",
		"Videos" => "Video"
	);

	private static $allowed_children = array(
        'Artwork',
    );
	
	private static $create_table_options = array(
        'MySQLDatabase' => 'ENGINE=MyISAM'
    );
    
    private static $searchable_fields = array(
        'Title' => array('title' => 'Name'),
        'NoPrice'
    );
    
     private static $better_buttons_actions = array (
        'MPReSync',
        'MPReSyncImages'
    );
	
	private static $summary_fields = array (
	   "Contact.LastName" => "Last Name",
	   "Contact.FirstName" => "First Name",
	   "Represented" => "Represented"
    );
	
	
	/**
	 * Common methods
	 * ----------------------------------*/
	
	public function getCMSFields() {
		$fields = parent::getCMSFIelds();
		
		$statsTitle = "Artist Details";
		$contactField = null;
		
		//Select Existing Contact or Create New One
		if(!$this->ContactID || $this->ContactID == 0) {
			$statsTitle = $this->Contact()->FirstName.' '.$this->Contact()->LastName.' Details';
			
			$artists = ContactGroup::get()->filter(array("Code" => "Artist"))->first()->Contacts()->filter(array('ArtistPageID' => 0));
			
			if($artists->count()) {
				
				$contactField = DropdownField::create(
					"ContactID", 
					"Contact", 
					$artists->map('ID', 'Title')
				)->setEmptyString('(Select One or Fill In Name Bellow)');
				
			}
		} else {
			$statsTitle = $this->Title.' Details';
			$contactField = ReadonlyField::create('ContactID', 'Contact ID');
		}
			
		$statusField = ToggleCompositeField::create(
	 		"ArtistGroup",
	 		$statsTitle,
	 		array(
	 			$contactField->addExtraClass('noborder'),
	 			TextField::create('Contact-_1_-FirstName', 'First Name')->addExtraClass('noborder'),
	 			TextField::create('Contact-_1_-LastName', 'Last Name')->addExtraClass('noborder'),
	 			ReadonlyField::create('Title', 'Page name', $this->Title)->addExtraClass('noborder'),
	 			DateField::create('BirthDate')->addExtraClass('noborder'),
	 			DateField::create('DeathDate')->addExtraClass('noborder'),
	 			CheckboxField::create('IsRepresented')
	 		)
	 	);
		
		$fields->removeFieldsFromTab("Root.Main", array("Title","MenuTitle"));
		
		$fields->insertBefore($statusField, "Content");
		$fields->insertBefore(HTMLEditorField::create("ShortBio")->setRows(4), "Content");
		$fields->renameField("Content", "CV");
		//Contact Fields
		//$fields->addFieldsToTab("Root.Main", ReadonlyField::create('ContactID', 'Contact ID'));
		
		if($this->ID != 0) {
			$fields->addFieldsToTab("Root.Main", UploadField::create("Picture", "Picture ".$this->PictureID)
                ->setFolderName('Artists/'.$this->URLSegment));
			
			
			$artworkFieldConfig = GridFieldConfig_RelationEditor::create();
			$artworkFieldConfig->addComponent(new GridFieldOrderableRows('Sort'));
			
			
			$artworkField = GridField::create("Artworks", "Artworks",$this->Artworks());
			$artworkField->setConfig($artworkFieldConfig);
			
			$fields->addFieldsToTab("Root.Artwork", $artworkField);
			
			$videoFieldConfig = GridFieldConfig_RelationEditor::create();
			
			$videoField = GridField::create("Videos", "Video", $this->Videos());
			
			$videoField->setConfig($videoFieldConfig);
			
			$fields->addFieldsToTab("Root.Videos", $videoField);			
		}
 		
		
		
		
		
		
		
		return $fields;
	}
	
	public function getBetterButtonsUtils() {
        $fields = parent::getBetterButtonsUtils();
        
        if($this->ID != 0) {
            /*
$fields->push(
            	BetterButtonCustomAction::create('MPReSync', 'Re-sync')
            		->setRedirectType(BetterButtonCustomAction::REFRESH)
					->setSuccessMessage('Synced Please Re-Publish')
            
            );
*/
            
             $fields->push(
            	BetterButtonCustomAction::create('MPReSyncImages', 'Get Picture')
            		->setRedirectType(BetterButtonCustomAction::REFRESH)
					->setSuccessMessage('Images Downloaded Please Re-attach')

            );
        }
        
        return $fields;
    }
    
	
	/**
	 * Accessor methods
	 * ----------------------------------*/
	
	/**
	 * Controller actions	
	 * ----------------------------------*/
	
	
	
	/**
	 * Template accessors
	 * ----------------------------------*/
	
	public function FirstThumb() {
		
		if($this->getValidArtworks()) {
			
			foreach ($this->getValidArtworks() as $art) {
				if($art->OrderedImages()->count() && $art->Quantity > 0) {
					return $art->OrderedImages()->First();
					
				} else {
					continue;
				}
			}
			
			return false;
		} else {
			return false;
		}
		
	}
	
	public function getValidArtworks() {
		$artworks = $this->Artworks()->filter(array("Quantity:GreaterThan" => 0));
		
		return $artworks->count() ? $artworks : false;
		
	}
	
	
	public function getLastName() {
		if($this->ContactID != 0 ) {
			return $this->Contact()->LastName;
		} else {
			return false;
		} 
	}
	
	public function getRepresented() {
		return $this->IsRepresented ? "Yes" : "No";
	}
	
	/**
	 * Object methods
	 * ----------------------------------*/
	 
	//DO NOT USE
	public function MPReSync() {
		set_time_limit ( 30 );
		//$records = MasterpieceUtils::getMasterpeiceItem("Artwork", "AND title.iTitleNum = ".$this->MPTitleID);
		
		if($records) {
			
			foreach($records as $record) {
				//Debug::show($change);
				if ( !empty($record->Error) )
					throw new Exception($record->Error);
				
				if( !$record->TitleId || $record->TitleId == "0" ) {
					echo "No Changes";
					continue;
				}
				
				$changes = MasterpieceUtils::ArtworkBridge($this, $record);
			}
			
			
			$this->write();
			echo "Written Artwork<br>";
			//$this->publish('Stage', 'Live');
			$this->flushCache();
			
		}
		
		return true;
	}
	
	Public function MPReSyncImages() {
		
		if($this->Picture() && $this->MPHasImage) {
			
			$this->PictureID = 0;
			
		}
		
		if($this->MPHasImage) {
			$image = MasterpieceUtils::GetArtistPhoto($this);
			if($image) {
				//echo "New Id = ".$image->ID."<br>";
				$this->PictureID = $image->ID;
				$this->write();
				$this->doPublish();
				$this->flushCache();
			
			}
		}
		
		
		
		return true;
		
	}

	public function onBeforeWrite() {
		
		if($this->Artworks()->count()) {
			$children = $this->Artworks()->exclude("NoPrice", $this->NoPrice);
			
			
			if($children->count()) {
				foreach($children as $child) {
					
					//$published = $child->isPublished();
					
					$child->NoPrice = $this->NoPrice;
					
					$child->write();
					
				}
			}
			
		}
		
		if($this->ID != 0 && $this->BlogTagID == 0) {
			$tag = BlogTag::get()->filter(array("Title" => $this->Title))->first();
			
			if($tag) {
				$this->BlogTagID = $tag->ID;
			} else {
				$tag = BlogTag::create();
				$tag->Title = $this->Title;
				$tag->write();
				
				$this->BlogTagID = $tag->ID;
			}
			
		}
		
		
		parent::onBeforeWrite();
	}
	
	public function onAfterWrite() {
		
		parent::onAfterWrite();
		if($this->Contact()) {
			$newTitle = $this->Contact()->FirstName.' '.$this->Contact()->LastName;
			
			if($this->Title != $newTitle) {
				$this->Title = $newTitle;
				$this->URLSegment = SiteTree::GenerateURLSegment($this->Title);
				
				$this->write();
			}
		}
		
		
		
	}
	
	public function UpdatePicture() {
		$artist = Artist::get()->byID($this->ID);
		echo "This Picture ID = ".$this->PictureID."<br>";
		echo "Artist Picture ID = ".$artist->PictureID."<br>";
		//Delete Current Image
		if($this->PictureID != 0) {
			echo "Delete existing picture<br>";
			$picture = Image::get()->byID($this->PictureID);
			echo $picture->ID."<br>";
			if($picture) {
				echo "Picture Found<br>";
				$picture->delete();
			}
			
			$this->PictureID = 0;
			echo $this->write(true);
			echo "<br>";
		}
		echo "Picture ID = ".$this->PictureID."<br>";
		$image = MasterpieceUtils::GetArtistPhoto($this);
		if(!$image) {
			echo "Image Failed to Download<br>";
		} else {
			echo "New Id = ".$image->ID."<br>";
			$this->PictureID = $image->ID;
			echo $this->write(true);
			echo "<br>";
			echo "Updated Artist<br>";
			echo $this->doPublish();
			echo "<br>";
			$this->flushCache();
			echo "Picture ID = ".$this->PictureID;
		
		}
		
		$artist= Artist::get()->byID($this->ID);
		echo "Picture ID = ".$artist->PictureID;

	}
	
	public function CMSEditLink() {
		return Controller::join_links("admin/artists/Artist/EditForm/field/Artist/item/".$this->ID."/edit");
	}
	
	public function NewsItems($count = null) {
		if($this->BlogTag()) {
			if ($this->BlogTag()->BlogPosts()->count()) {
				
				return $count ? $this->BlogTag()->BlogPosts()->limit($count) : $this->BlogTag()->BlogPosts();
				
				
			} else {
				return false;
			}
		} else {
			return false;
		}
		
	}

	
}


class Artist_Controller extends Page_Controller {
	
	private static $allowed_actions = array (
		'index',
		'PrevNextPage',
		'MPReSync',
		'MPReSyncImages',
		'Purchase',
		'view',
		'art',
		'doEmail',
		'EmailThis',
		'SyncArtistPicture',
		'HoldForm',
        'doHold',
		'InfoForm'
	);

	private static $manual_block_list = array(
	    array("Key" => "Email", "Value" => "nobles.edward326@gmail.com")
    );
	
	private static $url_handlers = array(
	    'art//$Action/$ID/$Name' => 'art'
	);

    private static $formArtwork;

    public function getBlocks() {
        return self::$manual_block_list;
    }

    /**
     * Artwork Controller Overide
     *
     * Display Artwork Data instead of Artist
     *
     * @param SS_HTTPRequest $request
     * @return HTMLText
     */
    public function art(SS_HTTPRequest $request) {
       	
       	$artwork = Artwork::get()->filter(
            array(
                "URLSegment" => $this->request->param('Action'),
                "ArtistID" => $this->ID
            )
        )->first();
       	
       	if($artwork) {
	    	Config::inst()->update('SSViewer', 'rewrite_hash_links', false);	
	       	$image = $artwork->OrderedImages()->first();
		
			if($image && $ratio = $image->getRatio()) {
				
				if($ratio > 1.2 ) {
					return $artwork->renderWith(array("ArtworkWide","Page"));
				} elseif ($ratio < 0.85) {
					return $artwork->renderWith(array("ArtworkNarrow","Page"));
				} else {
					return $artwork->renderWith(array("Artwork","Page"));
				}
				
			}
       	} else {
	       	echo "no Page";
       	}
       	
        // Your action code goes here
        
    }

    /**
     * Purchase Form
     *
     * Request to Purchase an Artwork. Currently no e-commerce option
     *
     * @return mixed
     */
    public function Purchase() {
	    
	    $actionArray = array('doPurchase', 'doHold', 'doInfo');
	    
	    if( !in_array($this->request->param('Action'), $actionArray) ) {
		    $artwork = Artwork::get()->filter(
                array(
                    "URLSegment" => $this->request->param('Action'),
                    "ArtistID" => $this->ID
                )
            )->first();
	    } else {
		    $artwork = false;
	    }
	    
	    if(!$artwork) {
		    $pageID = $this->ID;
	    } else {
		    $pageID = $artwork->ID;
	    }
	    
        $fields = FieldList::create(
            TextField::create('Name', false)
                ->setAttribute('placeholder', 'First and Last Name')
                ->setAttribute('required', true)
                ->addExtraClass('has-placeholder')
                ->setRightTitle("First and Last Name"),
            EmailField::create('Email', false)
                ->setAttribute('placeholder', 'Email')
                ->addExtraClass('has-placeholder half-field left-field')
                ->setRightTitle("Email")
                ->setAttribute('required', true),
            TextField::create('Phone', false)
                ->setAttribute('placeholder', 'Phone Number')
                ->setAttribute('pattern', '[+]?([0-9]*[\.\s\-\(\)]|[0-9]+){3,24}')
                ->setAttribute('title', 'A Valid International or North American Phone Number')
                ->addExtraClass('has-placeholder half-field')
                ->setRightTitle("Phone Number")
                ->setAttribute('required', true),
            TextField::create('Address', false)
                ->setAttribute('placeholder', 'Address')
                ->addExtraClass('has-placeholder')
                ->setRightTitle("Address"),
            TextField::create('Address2', false)
                ->setAttribute('placeholder', 'Address Line 2 (Optional)')
                ->addExtraClass('has-placeholder')
                ->setRightTitle("Address Line 2 (Optional)"),
            TextField::create('City', false)
                ->setAttribute('placeholder', 'City')
                ->addExtraClass('has-placeholder half-field left-field')
                ->setRightTitle("City"),
            TextField::create('Province', false)
                ->setAttribute('placeholder', 'State or Province')
                ->addExtraClass('has-placeholder half-field')
                ->setRightTitle("State or Province"),
            TextField::create('PostalCode', false)
                ->setAttribute('placeholder', 'Zip/Postal Code')
                ->addExtraClass('has-placeholder half-field left-field')
                ->setRightTitle("Zip/Postal Code"),
            TextField::create('Country', false)
                ->setAttribute('placeholder', 'Country')
                ->addExtraClass('has-placeholder half-field')
                ->setRightTitle("Country"),
            CheckboxField::create('Confirm', 'I agree to purchase this work')
                ->setAttribute('required', null)
                ->addExtraClass(' left-field half-field'),
            HiddenField::create('ArtworkID', null, $pageID)
        );

        $required = new RequiredFields(array(
            'Name', 'Email', 'Phone'
        ));

        $form = Form::create(
            $this,
            "Purchase",
            $fields,
            new FieldList(
                FormAction::create('doPurchase', 'Submit')->addExtraClass("button primary")//->setDisabled(true)
            ),
            $required
        );

        if(Session::get('PurchaseForm')) {
            $form->loadDataFrom(Session::get('PurchaseForm'));
        } else {
            $form->loadDataFrom(Controller::curr()->request->postVars());
        }

        return $form->enableSpamProtection(array(
            'mapping' => array(
                'Name' => 'authorName',
                'Email' => 'authorMail'
            )
        ));
    }

    /**
     * Process Purchase form
     *
     * @param $data
     * @param Form $form
     * @return bool|SS_HTTPResponse
     */
    public function doPurchase($data, Form $form) {

        $artwork = Artwork::get()->byID($data['ArtworkID']);

        if(!$artwork) {
            $form->sessionMessage(
                'There was a problem with your request, please contact the gallery directly',
                'error'
            );
            return Controller::curr()->redirectBack();
        }

        //Manual Block
        foreach ($this->getBlocks() as $block) {
            if($data[$block['Key']] == $block['Value'] ) {
                $form->sessionMessage(
                    'There was a problem with your request, please contact the gallery directly',
                    'error'
                );
                return Controller::curr()->redirectBack();
            }
        }
		
		$clientEmail = new Email();
		$galleryEmail = new Email();
		
		$clientFrom = Config::inst()->get('SiteConfig', 'ClientEmailFrom');
		$galleryFrom = Config::inst()->get('SiteConfig', 'GalleryEmailFrom');
		$galleryTo = Config::inst()->get('SiteConfig', 'GalleryEmailTo');
		
		
		$clientEmail->setFrom($clientFrom)
				    ->setTo($data['Email'])
				    ->setSubject('Purchase info for '.$artwork->Title.' by '.$artwork->Artist()->Title)
				    ->setTemplate('CustomerEmailPurchase')
				    ->populateTemplate(new ArrayData(array(
				        'Client' => $data,
				        'Artwork' => $artwork,
				        'SiteConfig' => SiteConfig::current_site_config()
				    )));
				    
		$galleryEmail->setFrom($galleryFrom)
				    ->setTo($galleryTo)
				    ->setSubject('Purchase request for '.$artwork->Title.' by '.$artwork->Artist()->Title)
				    ->setTemplate('GalleryEMailPurchase')
				    ->populateTemplate(new ArrayData(array(
				        'Client' => $data,
				        'Artwork' => $artwork
				    )));

        $data['Confirm'] = 0;
		Session::set('PurchaseForm', $data);
		
		$clientEmail->send();
		$galleryEmail->send();
		
		return Controller::redirect('thank-you/');
		
		
	}

    /**
     * Hold Request Form
     *
     * Request to put an an Artwork on hold
     *
     * @return mixed
     */
    public function HoldForm() {
	    
	    $actionArray = array('doPurchase', 'doHold', 'doInfo');
	    
	    if( !in_array($this->request->param('Action'), $actionArray) ) {
		    $artwork = Artwork::get()->filter(
                array(
                    "URLSegment" => $this->request->param('Action'),
                    "ArtistID" => $this->ID
                )
            )->first();
	    } else {
		    $artwork = false;
	    }
	    
	    if(!$artwork) {
		    $pageID = $this->ID;
	    } else {
		    $pageID = $artwork->ID;
	    }
	    
        $fields = FieldList::create(
            TextField::create('Name', false)
                ->setAttribute('placeholder', 'First and Last Name')
                //->setAttribute('required', true)
                ->addExtraClass('has-placeholder')
                ->setRightTitle("First and Last Name"),
            EmailField::create('Email', false)
                ->setAttribute('placeholder', 'Email')
                ->addExtraClass('has-placeholder half-field left-field')
                ->setRightTitle("Email"),
            //->setAttribute('required', true),
            TextField::create('Phone', false)
                ->setAttribute('placeholder', 'Phone Number')
                ->setAttribute('pattern', '[+]?([0-9]*[\.\s\-\(\)]|[0-9]+){3,24}')
                ->setAttribute('title', 'A Valid International or North American Phone Number')
                ->addExtraClass('has-placeholder half-field')
                ->setRightTitle("Phone Number"),
            //->setAttribute('required', true),
            CheckboxField::create('Confirm', 'Please hold this work for me for 48 hours')
                ->setAttribute('required', null)
                ->addExtraClass(' left-field half-field'),
            HiddenField::create('ArtworkID', null, $pageID)
        );

        $required = new RequiredFields(array(
            'Name', 'Email', 'Phone', 'Confirm'
        ));

        $form = Form::create(
            $this,
            "HoldForm",
            $fields,
            new FieldList(
                FormAction::create('doHold', 'Submit')->addExtraClass("button primary")//->setDisabled(true)
            ),
            $required
        );

        if(Session::get('PurchaseForm')) {
            $form->loadDataFrom(Session::get('PurchaseForm'));
        } else {
            $form->loadDataFrom(Controller::curr()->request->postVars());
        }

        return $form;

        return $form->enableSpamProtection(array(
            'mapping' => array(
                'Name' => 'authorName',
                'Email' => 'authorMail'
            )
        ));
    }

    /**
     * Process Hold Form
     *
     * @param $data
     * @param Form $form
     * @return bool|SS_HTTPResponse
     */
    public function doHold($data, Form $form) {

        $artwork = Artwork::get()->byID($data['ArtworkID']);
        $artist = $artwork->Artist();

        if(!$artwork) {
            $form->sessionMessage(
                'There was a problem with your request, please contact the gallery directly',
                'error'
            );
            return Controller::curr()->redirectBack();
        }

        //Manual Block
        foreach ($this->getBlocks() as $block) {
            if($data[$block['Key']] == $block['Value'] ) {
                $form->sessionMessage(
                    'There was a problem with your request, please contact the gallery directly',
                    'error'
                );
                return Controller::curr()->redirectBack();
            }
        }


        $clientEmail = new Email();
        $galleryEmail = new Email();

        $clientFrom = Config::inst()->get('SiteConfig', 'ClientEmailFrom');
        $galleryFrom = Config::inst()->get('SiteConfig', 'GalleryEmailFrom');
        $galleryTo = Config::inst()->get('SiteConfig', 'GalleryEmailTo');


        $clientEmail->setFrom($clientFrom)
            ->setTo($data['Email'])
            ->setSubject('Hold Request for '.$artwork->Title.' by '.$artwork->Artist()->Title)
            ->setTemplate('CustomerEmailHold')
            ->populateTemplate(new ArrayData(array(
                'Client' => $data,
                'Artwork' => $artwork,
                'SiteConfig' => SiteConfig::current_site_config(),
                'Artist' => $artist
            )));

        $galleryEmail->setFrom($galleryFrom)
            ->setTo($galleryTo)
            ->setSubject('Hold request for '.$artwork->Title.' by '.$artwork->Artist()->Title)
            ->setTemplate('GalleryEMailHold')
            ->populateTemplate(new ArrayData(array(
                'Client' => $data,
                'Artwork' => $artwork
            )));

        $data['Confirm'] = 0;
        Session::set('PurchaseForm', $data);

        $clientEmail->send();
        $galleryEmail->send();

        return Controller::redirect('thank-you-hold/');


    }

    /**
     * Request Information Form
     *
     * Form intended to be used to request more information about a specific work.
     *
     * @return mixed
     */
    public function InfoForm() {
	    
	    $actionArray = array('doPurchase', 'doHold', 'doInfo');
	    
	    if( !in_array($this->request->param('Action'), $actionArray) ) {
		    $artwork = Artwork::get()->filter(
                array(
                    "URLSegment" => $this->request->param('Action'),
                    "ArtistID" => $this->ID
                )
            )->first();
	    } else {
		    $artwork = false;
	    }
	    
	    if(!$artwork) {
		    $pageID = $this->ID;
	    } else {
		    $pageID = $artwork->ID;
	    }
	    
        $fields = FieldList::create(
            TextField::create('Name', false)
                ->setAttribute('placeholder', 'First and Last Name')
                ->addExtraClass('has-placeholder')
                ->setRightTitle("First and Last Name"),
            EmailField::create('Email', false)
                ->setAttribute('placeholder', 'Email (required)')
                ->addExtraClass('has-placeholder half-field left-field')
                ->setRightTitle("Email")
                ->setAttribute('required', true),
            TextField::create('Phone', false)
                ->setAttribute('placeholder', 'Phone Number')
                ->setAttribute('pattern', '[+]?([0-9]*[\.\s\-\(\)]|[0-9]+){3,24}')
                ->setAttribute('title', 'A Valid International or North American Phone Number')
                ->addExtraClass('has-placeholder half-field')
                ->setRightTitle("Phone Number"),
            TextareaField::create('Message', 'Questions')
                ->addExtraClass('has-placeholder'),
            CheckboxField::create('Confirm', 'Please send me more info on this work')->setAttribute('required', null),
            CheckboxField::create('Subscribe', 'Please sign me up for the Oeno Gallery Newsletter'),
            HiddenField::create('ArtworkID', null, $pageID)
        );

        $required = new RequiredFields(array(
            'Email', 'Confirm'
        ));

        $form = Form::create(
            $this,
            "InfoForm",
            $fields,
            new FieldList(
                FormAction::create('doInfo', 'Submit')->addExtraClass("button primary")//->setDisabled(true)
            ),
            $required
        );

        if(Session::get('PurchaseForm')) {
            $form->loadDataFrom(Session::get('PurchaseForm'));
        } else {
            $form->loadDataFrom(Controller::curr()->request->postVars());
        }

        return $form->enableSpamProtection(array(
            'mapping' => array(
                'Name' => 'authorName',
                'Email' => 'authorMail',
                'Message' => 'body'
            )
        ));
    }

    /**
     * Process the Request Information Form
     *
     * @param $data
     * @param Form $form
     * @return bool|SS_HTTPResponse
     */
    public function doInfo($data, Form $form) {
        $artwork = Artwork::get()->byID($data['ArtworkID']);
        $artist = $artwork->Artist();

        if(!$artwork) {
            $form->sessionMessage(
                'There was a problem with your request, please contact the gallery directly. ArtworkID ='.$data['ArtworkID'],
                'error'
            );
            return Controller::curr()->redirectBack();
        }

        //Manual Block
        
        foreach ($this->getBlocks() as $block) {
            if($data[$block['Key']] == $block['Value'] ) {
                $form->sessionMessage(
                    'There was a problem with your request, please contact the gallery directly',
                    'error'
                );
                return Controller::curr()->redirectBack();
            }
        }
		
		$galleryEmail = new Email();
		
		$clientFrom = Config::inst()->get('SiteConfig', 'ClientEmailFrom');
		$galleryFrom = Config::inst()->get('SiteConfig', 'GalleryEmailFrom');
		$galleryTo = Config::inst()->get('SiteConfig', 'GalleryEmailTo');
				    
		$galleryEmail->setFrom($galleryFrom)
				    ->setTo($galleryTo)
				    ->setSubject('Info request for '.$artwork->Title.' by '.$artwork->Artist()->Title)
				    ->setTemplate('GalleryEMailInfo')
				    ->populateTemplate(new ArrayData(array(
				        'Client' => $data,
				        'Artwork' => $artwork,
                        'Artist' => $artist
				    )));

        $data['Confirm'] = 0;
        Session::set('PurchaseForm', $data);
		
		$galleryEmail->send();


		if(key_exists('Subscribe', $data) ) {
		    $this->doSubscribe($data);
        }
		
		return Controller::redirect('thank-you-info/');

	}

    /**
     * Email Artwork Form
     *
     * Intended to be used to email a link to the current page to a friend.
     *
     * @return mixed
     */
    public function EmailThis(){
        $fields = FieldList::create(
            TextField::create('From', false)
                ->setAttribute('placeholder', 'Your Name')
                ->setAttribute('required', true)
                ->addExtraClass('has-placeholder')
                ->setRightTitle("Your Name"),
            EmailField::create('SenderEmail', false)
                ->setAttribute('placeholder', 'Your Email')
                ->addExtraClass('has-placeholder')
                ->setRightTitle("Your Email")
                ->setAttribute('required', true),
            EmailField::create('TargetEmail', false)
                ->setAttribute('placeholder', 'To (email)')
                ->addExtraClass('has-placeholder')
                ->setRightTitle("To (email)")
                ->setAttribute('required', true),
            TextareaField::create("Message"),
            HiddenField::create('ArtworkID', null, $this->ID)
        );

        $required = new RequiredFields(array(
            'From', 'SenderEmail', 'TargetEmail'
        ));

        $form = Form::create(
            Controller::curr()->getFormOwner(),
            "EmailThis",
            $fields,
            new FieldList(
                FormAction::create('doEmail', 'Send')->addExtraClass("button primary")->setDisabled(true)
            ),
            $required
        );

        return $form->enableSpamProtection(array(
            'mapping' => array(
                'Name' => 'authorName',
                'Email' => 'authorMail',
                'Message' => 'body'
            )
        ));

    }

    /**
     * Process Email Form
     *
     * @param $data
     * @param Form $form
     * @return SS_HTTPResponse
     */
    public function doEmail($data, Form $form) {
		
		//Debug::show($data);
		
		$artwork = Artwork::get()->byID($data['ArtworkID']);
        $artist = $artwork->Artist();

        if(!$artwork) {
            $form->sessionMessage(
                'There was a problem with your request, please contact the gallery directly',
                'error'
            );
            return $this->controller->redirectBack();
        }
		
		$clientEmail = new Email();
		
		
		$clientEmail->setFrom('info@oenogallery.com')
				    ->setTo($data['TargetEmail'])
				    ->setSubject($data['From'].' has an Artwork form Oeno Gallery')
				    ->setTemplate('CustomerEmailShare')
				    ->populateTemplate(new ArrayData(array(
				        'Client' => $data,
				        'Artwork' => $artwork,
				        'SiteConfig' => SiteConfig::current_site_config(),
                        'Artist' => $artist
				    )));
				    
		
		$clientEmail->send();
		
		return Controller::redirect($artwork->AbsoluteLink());
		
		
	}


    /**
     * Get and/or Set the $formArtwork static
     *
     * @return bool/DataObject
     */
    public function getFormArtwork() {
        if($this->formArtwork) {
            return $this->formArtwork;
        } else {
            $artwork = Artwork::get()->filter(
                array(
                    "URLSegment" => $this->request->param('Action'),
                    "ArtistID" => $this->ID
                )
            )->first();

            if($artwork) {
                $this->formArtwork = $artwork;
            } else {
                return false;
            }

        }
    }

	
	public function SyncArtistPicture() {
		if($this->MPHasImage){
			echo Artist::UpdatePicture();
		}
	}

	public function doSubscribe($data) {

        $auth = base64_encode( 'user:d5fb0a11b027730f0c7eb82ec73ebf8b-us13' );


        //Check if email exists
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://us13.api.mailchimp.com/3.0/lists/9838837fde/members/'. md5($data['Email']));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Basic '.$auth));
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $json = json_decode($result);

        $firstName = '';
        $lastName = '';

        if (key_exists('Name', $data)) {
            // check for space
            if(strpos($data['Name'], ' ') !== false) {
                $nameArray = explode(' ', $string, 2);
                $firstName = $nameArray[0];
                $lastName = $nameArray[1];
            } else {
                $firstName = $data['Name'];
            }



        }

        if($json->{'status'} == 404) {
            // if no member add to list

            $submitdata = array(
                'apikey'        => 'd5fb0a11b027730f0c7eb82ec73ebf8b-us13',
                'email_address' => $data['Email'],
                'status'        => 'subscribed',
                'merge_fields'  => array(
                    'FNAME' => $firstName,
                    'LNAME' => $lastName
                ),
                'interests'     => array('6bc7f05108' => true)
            );
            $json_data = json_encode($submitdata);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://us13.api.mailchimp.com/3.0/lists/273e7a8620/members/');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                'Authorization: Basic '.$auth));
            curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
            $result = curl_exec($ch);
        } elseif ($json->{'status'} == 400 && $json->{'title'} == 'Member Exists') {

            //if member exist then update
            $submitdata = array(
                'apikey'        => 'd5fb0a11b027730f0c7eb82ec73ebf8b-us13',
                //'email_address' => $data['Email'],
                //'status'        => 'subscribed',
                'merge_fields'  => array(
                    'FNAME' => $firstName,
                    'LNAME' => $lastName
                ),
                'interests'     => array('6bc7f05108' => true)
            );

            $json_data = json_encode($submitdata);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://us13.api.mailchimp.com/3.0/lists/9838837fde/members/'. md5($data['Email']));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                'Authorization: Basic '.$auth));
            curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
            $result = curl_exec($ch);
        }

        $json = json_decode($result);

        return;
    }
	
	
}