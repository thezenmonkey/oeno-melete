<?php

class ExhibitHolder extends Page {
	
	/**
	 * Static vars
	 * ----------------------------------*/
	
	

	/**
	 * Object vars
	 * ----------------------------------*/
	
	
	
	/**
	 * Static methods
	 * ----------------------------------*/
	
	private static $allowed_children = array(
        'Exhibit'
    );
	
	/**
	 * Data model
	 * ----------------------------------*/

	private static $db = array (
		
	);
	

	private static $has_one = array (
		
	);
	
	private static $has_many = array (
		
	);
	
	/**
	 * Common methods
	 * ----------------------------------*/
	
	public function getCMSFields() {
		$fields = parent::getCMSFIelds();
		
/*
		if($this->Artworks()) {
			$fields->addFieldsToTab("Root.Main", DropdownField("FeatureArtworkID", "Featured Artwork", $this->Artworks()->map("ID", "Title")), "Summary")
		}
*/
		
		$pages = SiteTree::get()->filter(array(
			'ParentID' => $this->ID,
			'ClassName' => 'Exhibit'
		));
		$gridField = new GridField(
			"Ex",
			'Exhibitions',
			$pages->sort('Created DESC'),
			GridFieldConfig_Lumberjack::create()
		);

		$tab = new Tab('Exhibit', 'Exhibition', $gridField);
		$fields->insertAfter($tab, 'Main');
		
		$fields->removeFieldFromTab('Root', 'ChildPages');
		
		
		return $fields;
	}
	
	/**
	 * Accessor methods
	 * ----------------------------------*/
	
	
	
	/**
	 * Controller actions	
	 * ----------------------------------*/
	
	
	
	/**
	 * Template accessors
	 * ----------------------------------*/
	
	public function getExhibitions() {
		
		$exhibitions = Exhibit::get()->filter(array("ParentID" => $this->ID))->sort("StartDate", "DESC");
		
		return $exhibitions->count() ? $exhibitions : false;
	}
	
	public function getCurrentExhibition() {
		$today = date('Y-m-d');
		
		$exhibition = Exhibit::get()->filter(
			array(
				"ParentID" => $this->ID,
				"StartDate:LessThanOrEqual" => $today,
				"EndDate:GreaterThanOrEqual" => $today
			)
		)->sort(array("OnGoing" => "AsC", "StartDate" => "DESC"))->first();
		
		return ($exhibition) ? $exhibition : false;
	}
	
	public function getCurrentExhibitions() {
		$today = date('Y-m-d');
		
		$exhibition = Exhibit::get()->filter(
			array(
				"ParentID" => $this->ID,
				"StartDate:LessThanOrEqual" => $today,
				"EndDate:GreaterThanOrEqual" => $today
			)
		)->sort(array("OnGoing" => "AsC", "StartDate" => "DESC"));
		
		return ($exhibition) ? $exhibition : false;
	}
	
	public function getUpcomingExhibitions() {
		$today = date('Y-m-d');
		
		$exhibitions = Exhibit::get()->filter(array("ParentID" => $this->ID, "StartDate:GreaterThan" => $today))->sort("StartDate", "ASC");
		
		return $exhibitions->count() ? $exhibitions : false;
	}
	
	public function getPastExhibitions() {
		$today = date('Y-m-d');
		
		$exhibitions = Exhibit::get()->filter(array("ParentID" => $this->ID, "EndDate:LessThan" => $today))->sort("EndDate", "DESC");
		
		return $exhibitions->count() ? $exhibitions : false;
	}
	
	
	/**
	 * Object methods
	 * ----------------------------------*/

	function requireDefaultRecords() {
		if(!SiteTree::get()->filter(array("ClassName" => "ExhibitHolder"))->First()){
			$page = new ExhibitHolder();
			$page->Title = "Exhibitions";
			$page->URLSegment = "exhibitions";
			$page->Sort = 1;
			$page->write();
			$page->publish('Stage', 'Live');
			$page->flushCache();
			DB::alteration_message('Exhibit Holder created', 'created');
		}
	
		parent::requireDefaultRecords();
	}

	
}


class ExhibitHolder_Controller extends Page_Controller {
	
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
		
	}
	
}